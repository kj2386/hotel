import {createApp} from 'vue';
import App from './App.vue';
import './styles/core/os.config.scss';

createApp(App)
  .mount('#app');
