/* eslint-disable */

export const BUTTON_TYPE = {
  icon: 'icon',
  primary: 'primary',
};

export const MAIN = {
  title: 'Luxury Hotels starting from $99',
  subtitle: 'Compare prices from 200+ booking sites to help you find the lowest price on the right hotel for you.',
  button: 'compare hotels',
  list: [
    {
      icon: 'place',
      text: 'choose city'
    },
    {
      icon: 'lock',
      text: 'Secure Platform'
    },
    {
      icon: 'food',
      text: 'Breakfast Included'
    },
    {
      icon: 'globe',
      text: '+150 Destinations'
    },

  ]
};

export const DISCOVER = {
  header: 'DISCOVER THE ENTIRE WORLD',
  title: 'Choose Your Destination',
  subtitle: 'The best text style you can find on themeforest.',
  list: [
    {
      img: 'https://pixfort.com/item/megapack/images/hotel/paris-icon.png',
      alt: 'Paris',
      title: 'Visit Paris',
      subtitle: 'Here\'s a free video course on how to use PixFort, add new content.',
      button: 'view deals'
    },
    {
      img: 'https://pixfort.com/item/megapack/images/hotel/rome-icon.png',
      title: 'Rome Trip',
      alt: 'Rome',
      subtitle: 'Here\'s a free video course on how to use PixFort, add new content.',
      button: 'view deals'
    },
    {
      img: 'https://pixfort.com/item/megapack/images/hotel/sidney-icon.png',
      alt: 'Sydney',
      title: 'Sydney Offers',
      subtitle: 'Here\'s a free video course on how to use PixFort, add new content.',
      button: 'view deals'
    },
  ]
};

export const PRICES = {
  header: 'BEST PRICES FOR OUR CLIENTS',
  title: 'Compare Best Hotels Prices',
  subtitle: 'The best text style you can find on themeforest.',
  list: [
    {
      img: 'https://pixfort.com/item/megapack/images/hotel/hotel-image-1.jpg',
      alt: 'Athens',
      title: 'Hotel in Athens',
      subtitle: 'STARTING FROM $199'
    },
    {
      img: 'https://pixfort.com/item/megapack/images/hotel/hotel-image-2.jpg',
      alt: 'Dubai',
      title: 'Hotel in Dubai',
      subtitle: 'STARTING FROM $259'
    },
    {
      img: 'https://pixfort.com/item/megapack/images/hotel/hotel-image-3.jpg',
      alt: 'Paris',
      title: 'Hotel in Paris',
      subtitle: 'STARTING FROM $339'
    },
  ]
};

export const JOIN_US = {
  title: 'More Than 5,000 Hotels',
  subtitle: 'From logo design to website designs and developers are ready.',
  button: 'join us today'
};

export const SPONSORS = [
  {
    img: 'https://pixfort.com/item/megapack/images/clients/bigcommerce.png',
    alt: 'Bigcommerce'
  },
  {
    img: 'https://pixfort.com/item/megapack/images/clients/foursquare.png',
    alt: 'FourSquare'
  },
  {
    img: 'https://pixfort.com/item/megapack/images/clients/popsugar.png',
    alt: 'PopSugar'
  },
  {
    img: 'https://pixfort.com/item/megapack/images/clients/surveymonkey.png',
    alt: 'SurveyMonkey'
  },
];

export const REVIEW = {
  quote: '" Lorem ipsum dolor sit amet consectet adipiscing elit sed do moda tempo incididunt ut labore et dolore magnar aliqua. "',
  name: 'John Clark',
  site: 'via Tripadvisor.com',
  img: 'https://pixfort.com/item/megapack/images/hotel/testimonials-man.jpg',
  alt: 'John Clark'
};

export const FORM = {
  header: 'HIGH CONVERSION FORM',
  title: 'Get The Best Hotel Prices & Deals',
  subtitle: 'The best text style you can find on themeforest.',
  footer: '*Don’t Worry! We hate SPAM as much as you do!',
  button: 'Subscribe For Updates',

};

export const PLACEHOLDER_TEXT = {
  fullName: 'enter your full name',
  email: 'enter your email address',
  phone: 'enter your phone number',
  plan: 'choose a plan'
};

export const CONTACT_US = {
  title: 'Contact Us',
  subtitle: 'We provides you with a full user management functionality that results in faster development, faster revenue, user and the ability to serve your users better engaging efficiently.',
  button: 'get in touch'
};

export const VISIT_US = {
  title: 'Visit us',
  address: '16 Rue Saint Paul, Paris',
  email: 'megapack@gmail.com',
  phone: '+33 4 35 62 48'
};

export const SOCIAL = {
  title: 'Stay Connected',
  subtitle: 'Find us on social network'
};
