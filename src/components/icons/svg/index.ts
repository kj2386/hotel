import HoPlace from './HoPlace.vue';
import HoLock from './HoLock.vue';
import HoFood from './HoFood.vue';
import HoGlobe from './HoGlobe.vue';
import HoRetinus from './HoRetinus.vue';
import HoFacebook from './HoFacebook.vue';
import HoTwitter from './HoTwitter.vue';
import HoInstagram from './HoInstagram.vue';

export default {
  HoPlace,
  HoLock,
  HoFood,
  HoGlobe,
  HoRetinus,
  HoFacebook,
  HoTwitter,
  HoInstagram,
};
